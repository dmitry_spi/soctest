﻿using SocTest.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocTest.Services
{
    public interface IQuestionService
    {
        List<Question> GetQuestionList();
        string CheckAnswers(List<Answer> answers);
    }
}
