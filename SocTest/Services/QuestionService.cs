﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SocTest.Models;

namespace SocTest.Services
{
    public class QuestionService : IQuestionService
    {
        DataContext _context;

        public QuestionService(DataContext context)
        {
            _context = context;
        }

        public string CheckAnswers(List<Answer> answers)
        {
            string result;
            int RatIrrat = 0;
            int LogicsEthics = 0;
            int SensIntuit = 0;
            int ExtrvrIntrvr = 0;
            var AllQuestions = _context.Questions.ToList();
            if (AllQuestions.Count != answers.Count)
            {
                return "";
            }
            var PositiveAnswers = answers.Where(a => a.NumOfAnswer == 1).ToList();
            foreach (var item in PositiveAnswers)
            {
                var question = AllQuestions.Where(q => q.Id == item.QuestionId).First();
                switch (question.Type)
                {
                    case QuestionType.RatIrrat:
                        RatIrrat++;
                        break;
                    case QuestionType.LogicsEthics:
                        LogicsEthics++;
                        break;
                    case QuestionType.SensIntuit:
                        SensIntuit++;
                        break;
                    case QuestionType.ExtrvrIntrvr:
                        ExtrvrIntrvr++;
                        break;
                    default:
                        break;
                }
            }
            string SocType="";
            if (RatIrrat > 8)
            {
                SocType += (LogicsEthics > 8 ? "Логико-" : "Этико-") + (SensIntuit > 8 ? "сенсорный " : "интуитивный ");
            }
            else
            {
                SocType += (SensIntuit > 8 ? "Cенсорно-" : "Интуитивно") + (LogicsEthics > 8 ? "логический " : "этический ");
            }
            SocType += ExtrvrIntrvr > 8 ? "экстраверт" : "интроверт";
            return SocType;
        }

        public List<Question> GetQuestionList()
        {
            return _context.Questions.OrderBy(q=>Guid.NewGuid()).ToList();
        }
    }
}
