﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SocTest.Models;
using SocTest.Services;

namespace SocTest.Controllers
{
    [Produces("application/json")]
    [Route("api/Question")]
    public class QuestionController : Controller
    {
        IQuestionService _questionService;
        DataContext _dataContext;

        public QuestionController(IQuestionService questionService, DataContext dataContext)
        {
            _questionService = questionService;
            _dataContext = dataContext;
        }

        [HttpPost("getquest")]
        public IActionResult GetQuestions()
        {
            var questions = _questionService.GetQuestionList();
            return Ok(questions);
        }

        public IActionResult CheckAnswers([FromBody]List<Answer> answers)
        {
            var result = _questionService.CheckAnswers(answers);
            if (result == "")
            {
                return BadRequest("AnswersIsWrong");
            }
            var user_id = HttpContext.Session.GetString("UserId");
            var user_result = new UserResults
            {
                User = _dataContext.Users.Where(u => u.Id == Guid.Parse(user_id)).First(),
                Result = result
            };
            _dataContext.UserResults.Add(user_result);
            _dataContext.SaveChanges();
            return Ok(result);
        }
    }
}
