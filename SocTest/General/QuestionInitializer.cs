﻿using SocTest.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace SocTest.General
{
    public class QuestionInitializer
    {
        public static void Init(DataContext context)
        {
            if (context.Questions.Count() != 0)
            {
                return;
            }
            string[] text_lines;
            try
            {
                text_lines = File.ReadAllLines(Directory.GetCurrentDirectory()+@"\General\Questions.txt");
            }
            catch (Exception ex)
            {
                // log error
                return;
            }
            for (int i = 0; i < text_lines.Length; i += 3)
            {
                var question = new Question
                {
                    Answer1 = text_lines[i],
                    Answer2 = text_lines[i + 1]
                };
                if (Int32.TryParse(text_lines[i + 2], out int quest_type))
                {
                    try
                    {
                        question.Type = (QuestionType)quest_type;
                    }
                    catch (Exception ex)
                    {
                        // log error
                        return;
                    }
                }
                context.Questions.Add(question);
            }
            context.SaveChanges();
        }
    }
}
